package co.com.sofka;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class StringCalculator {
    private String delimiter;
    private String[] delimitersArray;
    
    private String[] splittedList;

    public int add(String values) throws Exception {
        if(values.length() > 0) {
            delimiter = null;
            delimitersArray = new String[1];
            try {
                int temp = Integer.parseInt("" + values.charAt(0));
                delimitersArray[0] = null;
            } catch(Exception e) {
                if(("" + values.charAt(0)) == "-") {
                    delimitersArray[0] = null;
                } else if(values.charAt(0) == '['){
                    delimiter = values.substring(1, values.indexOf(']'));
                    String subStringValues = values.substring(0, values.lastIndexOf(']'));
                    delimitersArray = subStringValues
                            .replace('[', ' ')
                            .replace(']', ' ')
                            .trim()
                            .replaceAll("\\s+", ";").split(";");
                    Arrays.sort(delimitersArray, Collections.reverseOrder());
                } else {
                    delimiter = "" + values.charAt(0);
                    delimitersArray[0] = "" + values.charAt(0);
                }
            }

            split(values);

            return negativeNumber();
        }
        return 0;
    }

    private void split(String values) {
        if(delAndArEqOneNotNull()) {
            splittedList = values.substring(1).split(delimitersArray[0]);
            return;
        } else if (delAndArGtOneNotNull()){
            for (String del : delimitersArray) {
                values = values.replaceAll(del, "_");
            }
            splittedList = values.substring(values.lastIndexOf(']') + 1).split("_");
            return;
        }
        splittedList = values.split("[,|\n]");
    }

    private boolean delNotNull() {
        return delimiter != null;
    }

    private boolean delEqOne() {
        return delimiter.length() == 1;
    }

    private boolean delGtOne() {
        return delimiter.length() > 1;
    }

    private boolean delArEqOne() {
        return delimitersArray.length == 1;
    }

    private boolean delArGtOne() {
        return delimitersArray.length > 1;
    }

    private boolean delArNotNull() {
        return delimitersArray[0] != null;
    }

    private boolean delAndArEqOneNotNull(){
        return (delNotNull() && delEqOne()) && (delArEqOne() && delArNotNull());
    }

    private boolean delAndArGtOneNotNull(){
        return (delNotNull() && delGtOne()) || (delArNotNull() && delArGtOne());
    }

    private int negativeNumber() throws Exception {
        ArrayList<Integer> numberList = new ArrayList<>();
        int accumulator = 0;
        for(String element: splittedList) {
            int tempValue = Integer.parseInt(element);
            if(tempValue < 0) {
                throw new Exception("NegativeNumberException");
            }
            if(tempValue > 1000) {
                continue;
            }
            numberList.add(tempValue);
        }
        for(Integer number: numberList) {
            accumulator += number;
        }
        return accumulator;
    }
}
